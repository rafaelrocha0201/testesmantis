using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using CSharpSeleniumExtentReportNetCoreTemplate.DataBaseSteps;
using CSharpSeleniumExtentReportNetCoreTemplate.Helpers;
using CSharpSeleniumExtentReportNetCoreTemplate.Pages;
using NUnit.Framework;
using System.Collections;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Tests
{
    [TestFixture]
    public class TesteRealizarLogin : TestBase
    {

        #region Pages and Flows Objects
        MyViewPage myViewPage;
        Flows.LoginFlow loginFlow;
        #endregion

        [Test]
        public void RealizarLogin()
        {
            myViewPage = new MyViewPage();
            loginFlow = new Flows.LoginFlow();

            #region Parameters
            string usuario = "rafael.rocha";
            string senha = "RB02012003";
            #endregion

            loginFlow.EfetuarLogin(usuario, senha);

            string tituloCorreto = myViewPage.GetTitle();
            string tituloExperado = "My View - MantisBT";

            Assert.AreEqual(tituloExperado, tituloCorreto);
        }
        

    }
}
