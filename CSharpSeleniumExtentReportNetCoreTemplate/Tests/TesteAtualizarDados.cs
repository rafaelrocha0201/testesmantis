using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using CSharpSeleniumExtentReportNetCoreTemplate.DataBaseSteps;
using CSharpSeleniumExtentReportNetCoreTemplate.Helpers;
using CSharpSeleniumExtentReportNetCoreTemplate.Pages;
using NUnit.Framework;
using System.Collections;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Tests
{
    [TestFixture]
    public class TesteAtualizarDados : TestBase
    {

        #region Pages and Flows Objects
        MyViewPage myViewPage;
        LoginPage loginPage;
        MyAccountPage myAccountPage;
        Flows.LoginFlow loginFlow;
        #endregion

        [Test]
        public void AtualizarDados1()
        {
            myViewPage = new MyViewPage();
            loginPage = new LoginPage();
            myAccountPage = new MyAccountPage();
            loginFlow = new Flows.LoginFlow();

            #region Parameters
            string usuario = "rafael.rocha";
            string senha = "RB02012003";
            #endregion

            loginFlow.EfetuarLogin(usuario, senha);

            myViewPage.ClicarMyAccount();

            myAccountPage.PreencherConfirmPassword("RB02012003");
            //adicionar um wait
            myAccountPage.ClicarUpdate();

            string url = myAccountPage.GetURL();
            string urlExpected = "https://mantis-prova.base2.com.br/account_update.php";

            Assert.AreEqual(urlExpected, url);
        }
        
        [Test]
        public void AtualizarDados2()
        {
            myViewPage = new MyViewPage();
            loginPage = new LoginPage();
            myAccountPage = new MyAccountPage();
            loginFlow = new Flows.LoginFlow();

            #region Parameters
            string usuario = "rafael.rocha";
            string senha = "RB02012003";
            #endregion

            loginFlow.EfetuarLogin(usuario, senha);

            myViewPage.ClicarMyAccount();

            myAccountPage.PreencherConfirmPassword("RB02012003");
            //adicionar um wait
            myAccountPage.ClicarUpdate();

            myAccountPage.NavigateTo("https://mantis-prova.base2.com.br/my_view_page.php");
            myViewPage.ClicarLogout();

            string tituloCorreto = loginPage.GetTitle();
            string tituloExperado = "MantisBT";

            Assert.AreEqual(tituloExperado, tituloCorreto);
        }

    }
}
