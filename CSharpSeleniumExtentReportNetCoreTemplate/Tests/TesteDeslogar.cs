using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using CSharpSeleniumExtentReportNetCoreTemplate.DataBaseSteps;
using CSharpSeleniumExtentReportNetCoreTemplate.Helpers;
using CSharpSeleniumExtentReportNetCoreTemplate.Pages;
using NUnit.Framework;
using System.Collections;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Tests
{
    [TestFixture]
    public class TesteDeslogar : TestBase
    {

        #region Pages and Flows Objects
        MyViewPage myViewPage;
        LoginPage loginPage;
        Flows.LoginFlow loginFlow;
        #endregion

        [Test]
        public void Deslogar()
        {
            myViewPage = new MyViewPage();
            loginPage = new LoginPage();
            loginFlow = new Flows.LoginFlow();

            #region Parameters
            string usuario = "rafael.rocha";
            string senha = "RB02012003";
            #endregion

            loginFlow.EfetuarLogin(usuario, senha);
            myViewPage.ClicarLogout();

            string tituloCorreto = loginPage.GetTitle();
            string tituloExperado = "MantisBT";

            Assert.AreEqual(tituloExperado, tituloCorreto);
        }
        

    }
}
