using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using CSharpSeleniumExtentReportNetCoreTemplate.DataBaseSteps;
using CSharpSeleniumExtentReportNetCoreTemplate.Helpers;
using CSharpSeleniumExtentReportNetCoreTemplate.Pages;
using NUnit.Framework;
using System.Collections;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Tests
{
    [TestFixture]
    public class TesteVisualizarIssues : TestBase
    {

        #region Pages and Flows Objects
        MyViewPage myViewPage;
        LoginPage loginPage;
        ViewIssuePage viewIssuePage;
        Flows.LoginFlow loginFlow;
        #endregion

        [Test]
        public void VisualizarIssues()
        {
            myViewPage = new MyViewPage();
            loginPage = new LoginPage();
            viewIssuePage = new ViewIssuePage();
            loginFlow = new Flows.LoginFlow();

            #region Parameters
            string usuario = "rafael.rocha";
            string senha = "RB02012003";
            #endregion

            loginFlow.EfetuarLogin(usuario, senha);

            myViewPage.ClicarViewIssue();
            string tituloCorreto = viewIssuePage.GetTitle();
            string tituloExperado = "View Issues - MantisBT";

            Assert.AreEqual(tituloExperado, tituloCorreto);
        }
        

    }
}
