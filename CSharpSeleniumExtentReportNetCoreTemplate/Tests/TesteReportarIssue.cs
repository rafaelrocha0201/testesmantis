using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using CSharpSeleniumExtentReportNetCoreTemplate.DataBaseSteps;
using CSharpSeleniumExtentReportNetCoreTemplate.Helpers;
using CSharpSeleniumExtentReportNetCoreTemplate.Pages;
using NUnit.Framework;
using System.Collections;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Tests
{
    [TestFixture]
    public class TesteReportarIssue : TestBase
    {

        #region Pages and Flows Objects
        MyViewPage myViewPage;
        LoginPage loginPage;
        ReportIssuePage reportIssuePage;
        Flows.LoginFlow loginFlow;
        Flows.ReportarIssueFlow reportarIssueFlow;
        #endregion

        [Test]
        public void ReportarIssue1()
        {
            myViewPage = new MyViewPage();
            loginPage = new LoginPage();
            reportIssuePage = new ReportIssuePage();
            loginFlow = new Flows.LoginFlow();
            reportarIssueFlow = new Flows.ReportarIssueFlow();  

            reportarIssueFlow.ReportarIssue();

            string url = reportIssuePage.GetURL();
            string urlExpected = "https://mantis-prova.base2.com.br/bug_report.php";

            Assert.AreEqual(urlExpected, url);
        }

        [Test]
        public void ReportarIssue2()
        {
            myViewPage = new MyViewPage();
            loginPage = new LoginPage();
            reportIssuePage = new ReportIssuePage();
            loginFlow = new Flows.LoginFlow();
            reportarIssueFlow = new Flows.ReportarIssueFlow(); 

            reportarIssueFlow.ReportarIssue();

            reportIssuePage.NavigateTo("https://mantis-prova.base2.com.br/view_all_bug_page.php");

            string url = reportIssuePage.GetURL();
            string urlExpected = "https://mantis-prova.base2.com.br/view_all_bug_page.php";

            Assert.AreEqual(urlExpected, url);
        }

        [Test]
        public void ReportarIssue3()
        {
            myViewPage = new MyViewPage();
            loginPage = new LoginPage();
            reportIssuePage = new ReportIssuePage();
            loginFlow = new Flows.LoginFlow();
            reportarIssueFlow = new Flows.ReportarIssueFlow(); 

            reportarIssueFlow.ReportarIssue();

            reportIssuePage.NavigateTo("https://mantis-prova.base2.com.br/my_view_page.php");
            string url = reportIssuePage.GetURL();
            string urlExpected = "https://mantis-prova.base2.com.br/my_view_page.php";

            Assert.AreEqual(urlExpected, url);
        }
    }
}
