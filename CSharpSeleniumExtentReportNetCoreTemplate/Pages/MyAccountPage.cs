using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    public class MyAccountPage : PageBase
    {
        #region Mapping
        By Password = By.Name("password");
        By ConfirmPassword = By.Name("password_confirm");
        By Email = By.Name("email");
        By RealName = By.Name("realname");
        By UpdateButton = By.XPath("//input[@class='button']");
        #endregion

        #region Actions
        public void PreencherPassword(string password)
        {
            SendKeys(Password, password);
        }
        public void PreencherConfirmPassword(string confirmpassword)
        {
            SendKeys(ConfirmPassword, confirmpassword);
        }
        public void PreencherEmail(string email)
        {
            SendKeys(Email, email);
        }
        public void PreencherRealName(string realname)
        {
            SendKeys(RealName, realname);
        }
        public void ClicarUpdate()
        {
            Click(UpdateButton);
        }
        #endregion
    }
}