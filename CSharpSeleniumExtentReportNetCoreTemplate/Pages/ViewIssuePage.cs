using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    public class ViewIssuePage : PageBase
    {
        #region Mapping
        By SwitchProject = By.Name("project_id");
        #endregion

        #region Actions
        public void TrocarProjeto(string nomeProjeto)
        {
            ComboBoxSelectByVisibleText(SwitchProject, nomeProjeto);
        }
        #endregion
    }
}