using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    public class ReportIssuePage : PageBase
    {
        #region Mapping
        By Category = By.Name("category_id");
        By Reproducibillity = By.Name("reproducibility");
        By Severity = By.Name("severity");
        By Priority = By.Name("priority");
        By Profile = By.Name("profile_id");
        By Plataform = By.Id("Plataform");
        By OS = By.Id("os");
        By OSVersion = By.Id("os_build");
        By Summary = By.Name("summary");
        By Description = By.Name("description");
        By Steps = By.Name("steps_to_reproduce");
        By AdditionalInfo = By.Name("additional_info");
        By SubmitButton = By.XPath("//td//input[@value='Submit Report']");
        #endregion

        #region Actions
        public void SelecionarCategory()
        {
            ComboBoxSelectByVisibleText(Category, "[All Projects] Teste");
        }
        public void SelecionarReproducibillity()
        {
            ComboBoxSelectByVisibleText(Reproducibillity, "always");
        }
        public void SelecionarSeverity()
        {
            ComboBoxSelectByVisibleText(Severity, "minor");
        }
        public void SelecionarPriority()
        {
            ComboBoxSelectByVisibleText(Priority, "normal");
        }
        public void SelecionarProfile()
        {
            ComboBoxSelectByVisibleText(Profile, "Desktop Windows 10");
        }
        public void PreencherPlataform(string plataforma)
        {
            SendKeys(Plataform, plataforma);
        }
        public void PreencherOS(string os)
        {
            SendKeys(OS, os);
        }
        public void PreencherOSVersion(string os_version)
        {
            SendKeys(OSVersion, os_version);
        }
        public void PreencherSummary(string summary)
        {
            SendKeys(Summary, summary);
        }
        public void PreencherDescription(string description)
        {
            SendKeys(Description, description);
        }
        public void PreencherSteps(string steps)
        {
            SendKeys(Steps, steps);
        }
        public void PreencherAdditionalInfo(string addinfo)
        {
            SendKeys(AdditionalInfo, addinfo);
        }
        public void ClicarSubmit()
        {
            Click(SubmitButton);
        }
        #endregion
    }
}