using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    public class LoginPage : PageBase
    {
        #region Mapping
        By usuarioField = By.Name("username");
        By passwordField = By.Name("password");
        By loginButton = By.XPath("//td//input[@type='submit']");
        #endregion

        #region Actions
        
        public void PreencherUsuario(string email)
        {
            SendKeys(usuarioField, email);
        }

        public void PreencherSenha(string senha)
        {
            SendKeys(passwordField, senha);
        }

        public void ClicarEmLogin()
        {
            Click(loginButton);
        }
        #endregion
    }
}