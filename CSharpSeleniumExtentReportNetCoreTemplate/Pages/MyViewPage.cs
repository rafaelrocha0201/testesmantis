using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    public class MyViewPage : PageBase
    {
        #region Mapping
        By ReportIssueButton = By.XPath("//td//a[@href='/bug_report_page.php']");
        By ViewIssueButton = By.XPath("//td//a[@href='/view_all_bug_page.php']");
        By MyAccountButton = By.XPath("//td//a[@href='/account_page.php']");
        By LogoutButton = By.XPath("//td//a[@href='/logout_page.php']");
        #endregion

        #region Actions
        public void ClicarReportIssue()
        {
            Click(ReportIssueButton);
        }
        public void ClicarViewIssue()
        {
            Click(ViewIssueButton);
        }
        public void ClicarMyAccount()
        {
            Click(MyAccountButton);
        }
        public void ClicarLogout()
        {
            Click(LogoutButton);
        }
        #endregion
    }
}