using CSharpSeleniumExtentReportNetCoreTemplate.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Flows
{
    public class ReportarIssueFlow
    {
        #region Page Object and Constructor
        MyViewPage myViewPage;
        LoginPage loginPage;
        ReportIssuePage reportIssuePage;
        Flows.LoginFlow loginFlow;

        public ReportarIssueFlow()
        {
            myViewPage = new MyViewPage();
            loginPage = new LoginPage();
            reportIssuePage = new ReportIssuePage();
            loginFlow = new Flows.LoginFlow(); 
        }
        #endregion

        public void ReportarIssue()
        {
            #region Parameters
            string usuario = "rafael.rocha";
            string senha = "RB02012003";
            #endregion

            loginFlow.EfetuarLogin(usuario, senha);
            myViewPage.ClicarReportIssue();

            //selects
            reportIssuePage.SelecionarCategory();
            reportIssuePage.SelecionarReproducibillity();
            reportIssuePage.SelecionarSeverity();
            reportIssuePage.SelecionarPriority();
            reportIssuePage.SelecionarProfile();

            //inputs
            reportIssuePage.PreencherSummary("Teste");
            reportIssuePage.PreencherDescription("Teste de automação utilizando o mantis");
            reportIssuePage.PreencherSteps("Realizar o teste");
            reportIssuePage.PreencherAdditionalInfo("Nada para preencher");
            reportIssuePage.ClicarSubmit();

        }
    }
}
